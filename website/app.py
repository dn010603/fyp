from flask import Flask, render_template, request, redirect, url_for, session, request
import psycopg2
from models import users, Part, Student
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import join
from sqlalchemy.sql import text
from database import db

app = Flask(__name__)

# Configure the database connection
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:P553abc@localhost:5432/FinalYearProject'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Initialize the SQLAlchemy object
db.init_app(app)

# Test the database connection
try:
    with app.app_context():
        db.session.execute(text('SELECT 1'))
        print("Database connection successful")
except Exception as e:
    print("Error occurred during database connection:", str(e))



@app.route('/programme_and_module', methods=['GET', 'POST'])
def programme_and_module():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        
        # Perform authentication and retrieve student ID based on the username and password
        if username == 'valid_username' and password == 'valid_password':
            # Successful authentication, retrieve student data
            
            # Retrieve the student's ID from the 'Users' table
            user = Users.query.filter_by(username=username).first()
            
            if user:
                # Retrieve the student's data from the 'Student' table based on the user's ID
                student = db.session.query(Student).join(Users).filter(Users.id == user.id).first()
                
                if student:
                    # Retrieve the relevant data from the student object
                    part_number = student.partnumber
                    module_name = student.modulename
                    coursework_grade = student.courseworkgrade
                    exam_grade = student.examgrade
                    module_grade_overall = student.modulegradeoverall
                    student_id = student.studentid 
                    return render_template('student.html', part_number=part_number, module_name=module_name, coursework_grade=coursework_grade, exam_grade=exam_grade, module_grade_overall=module_grade_overall, student_id=student_id)
        
        # Invalid credentials, show error message
        error_message = "Invalid username or password"
        return render_template('programme_and_module.html', error_message=error_message)
    
    else:
        return render_template('programme_and_module.html')


@app.route('/personal_data', methods=['GET', 'POST'])
def personal_data():
    return render_template('personal_data.html')

@app.route('/welcome')
def welcome():
    return render_template('welcome.html')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/modules')
def modules():
    return render_template('modules.html')

@app.route('/teacher')
def teacher_page():
    return render_template('teacher.html')


from flask import request

@app.route('/student', methods=['GET', 'POST'])
def student():
    if request.method == 'POST':
        # Handle the POST request data
        username = request.form.get('username')
        password = request.form.get('password')
        
        # Perform any necessary validation or processing with the username and password
        
        if username == 'valid_username' and password == 'valid_password':
            # Successful login logic
            user = Users.query.filter_by(username=username).first()
            student = Student.query.join(Users).filter(Users.id==user.id).first()

            if student:
                module_name = student.modulename
                coursework_grade = student.courseworkgrade
                exam_grade = student.examgrade 
                module_grade_overall = student.modulegradeoverall

                return render_template('student.html', studentID=user_id, module_name=module_name, coursework_grade=coursework_grade, exam_grade=exam_grade, module_grade_overall=module_grade_overall)
            
        else:
         # Invalid credentials, show error message
            error = 'Invalid username or password. Please try again.'
            return render_template('student.html', error=error)
    
    # For GET requests or initial rendering of the page
    return render_template('student.html')


@app.route('/actions')
def actions():
    return render_template('actions.html')

@app.route('/applications')
def applications():
    return render_template('applications.html')


@app.route('/ask')
def ask():
    return render_template('ask.html') 

@app.route('/info')
def info():
    return render_template('info.html') 

@app.route('/timetable')
def timetable():
    return render_template('timetable.html') 

@app.route('/data')
def data():
    return render_template('data.html')




if __name__ == '__main__':
    app.run(debug=True, port=5001)
