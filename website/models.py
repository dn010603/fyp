# models.py
from database import db
from sqlalchemy.orm import relationship

class users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    duties = db.Column(db.String(120), nullable=False)
    
    # Add a one-to-one relationship between Users and Student
    student = relationship("Student", uselist=False, back_populates="users")

class Student(db.Model):
    __tablename__ = 'student'
    id = db.Column(db.Integer, primary_key=True)
    partnumber = db.column(db.Integer)
    modulename = db.Column(db.String(120))
    courseworkgrade = db.Column(db.Integer)
    examgrade = db.Column(db.Integer)
    modulegradeoverall = db.Column(db.Integer)
    studentID = db.Column(db.String(120), unique=True, nullable=False)
   
    # Add a foreign key constraint and a reference to the Users model
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    user = relationship("users", back_populates="student")

   
    
class Part(db.Model):
    __tablename__ = 'parts'
    id = db.Column(db.Integer, primary_key=True)
    module_code = db.Column(db.String(20), nullable=False)
    modules = db.Column(db.String(100), nullable=False)
    credits = db.Column(db.Integer, nullable=False)
    number_of_students = db.Column(db.Integer, nullable=False)
    lecturer = db.Column(db.String(100), nullable=False)
    school = db.Column(db.String(100), nullable=False)
    number_of_classes = db.Column(db.Integer, nullable=False)
    students = db.relationship('Student', backref='part', lazy=True)
